var crypto = require('crypto'),
    URL = 'https://api.cloudinary.com',
    API_VERSION = 'v1_1';

var CloudinaryUrl = function(apiKey, apiSecret, bucket){
  if (!apiKey) {
    throw new Error('an apiKey is required');
  }
  if (!apiSecret) {
    throw new Error('an apiSecret key is required');
  }
  if (!bucket) {
    throw new Error('a bucket is required');
  }
  this.apiKey = apiKey;
  this.apiSecret = apiSecret;
  this.bucket = bucket;
}

CloudinaryUrl.prototype.generateSignature = function(params){
  var paramsToSign = [], shasum

  params || (params = {});

  ['public_id', 'timestamp'].forEach(function(key){
    if (params[key]){
      paramsToSign.push('' + key + '=' + params[key]);
    }
  });

  shasum = crypto.createHash('sha1');
  shasum.update(paramsToSign.sort().join('&') + this.apiSecret);
  return shasum.digest('hex')
};

CloudinaryUrl.prototype.generateUrl = function(){
  return [URL, API_VERSION, this.bucket, 'image/upload'].join('/')
};

CloudinaryUrl.prototype.sign = function(params){
  params || (params = {})

  params.timestamp || (params.timestamp = new Date().getTime());
  params.api_key || (params.api_key = this.apiKey);
  params.signature = this.generateSignature(params);

  return {
    url: this.generateUrl(),
    params: params
  }
};

module.exports = CloudinaryUrl;