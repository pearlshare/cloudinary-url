var expect = require('expect.js'),
    CloudinaryUrl = require('../'),
    generator,
    apiKey = 'xxx',
    apiSecret = 'yyy',
    bucket = 'demo'


describe('cloudinaryUrl', function(){


  it('should be a constructor function', function () {
    expect(CloudinaryUrl).to.be.a('function')
  });

  it('should throw an error if no apiKey is given', function () {
    try {
      new CloudinaryUrl();
    } catch (e) {
      expect(e.message).to.match(/apiKey/);
    }
  });

  it('should throw an error if no apiSecret is given', function () {
    try {
      new CloudinaryUrl(apiKey);
    } catch (e) {
      expect(e.message).to.match(/apiSecret/);
    }
  });

  it('should throw an error if no bucket is given', function () {
    try {
      new CloudinaryUrl(apiKey, apiSecret);
    } catch (e) {
      expect(e).to.match(/bucket/);
    }
  });

  describe('url generator instances', function () {
    generator = new CloudinaryUrl(apiKey, apiSecret, bucket);

    it('should have a sign method', function () {
      expect(generator.sign).be.a('function')
    });

    describe('sign', function(){
      it('should return an object', function () {
        var out = generator.sign()
        expect(out).to.be.an('object')
        expect(out.url).to.match(/api\.cloudinary\.com/);
        expect(out.params).to.be.a('object')
        expect(out.params.timestamp).to.be.a('number');
        expect(out.params.api_key).to.be.a('string').and.to.equal(apiKey);
        expect(out.params.signature).to.be.a('string');
      });
    });

    describe('generateSignature', function(){
      it('should generate a signature with a public_id', function () {
        var params = {
          timestamp: 1417712334939,
          api_key: apiKey,
          public_id: 'fish'
        }
        var signature = generator.generateSignature(params)

        expect(signature).to.equal('2668545e42135090029f7c985b85af7f9c18744e')
      });

      it('should generate a signature without a public_id', function () {
        var params = {
          timestamp: 1417712334939,
          api_key: apiKey
        }
        var signature = generator.generateSignature(params)

        expect(signature).to.equal('7d8a63c62b0f87e219a281bc0d796127bc053bb7')
      });
    });


  });


});